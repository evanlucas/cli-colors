BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
PINK=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
GRAY=$(tput setaf 8)
BOLD=$(tput bold)
RESET=$(tput sgr0)
printError() {
	echo -e "${RED}[ERROR]:: $1"
	echo -ne "${RESET}"
}

printWarning() {
	echo -e "${YELLOW}[WARNING]:: $1"
	echo -ne "${RESET}"
}


printNotice() {
	echo -e "${CYAN}[NOTICE]:: $1"
	echo -ne "${RESET}"
}


